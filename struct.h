#define MAXBUF 1024

/*Связный список -> очередь сообщений*/
struct trick
{
	char message[MAXBUF];
	struct trick *previous;
	struct trick *next;
};
//char* Get_Queue(void);

/*Данные для передачи по TCP*/
struct data
{
	int time;
	int randString;
	char string[MAXBUF];
};


struct socket
{
    int server_port;
    char ip_adress[MAXBUF];
};