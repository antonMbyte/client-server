#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h> 
#include <stdlib.h> 
#include <pthread.h>
#include <errno.h>
#include <wait.h>
#include <string.h>
#include <arpa/inet.h>
#include "struct.h"
#include "interfaces.h"
#include "amessage.pb-c.h"

struct data msg_tcp;
struct socket socket_recv;
struct sockaddr_in addr;
struct sockaddr_in sockAddr;

pthread_t threads_udp;
pthread_t threads_tcp; 

int udp_msg_status = -1;

AMessage *proto_msg;//указатель на распакованную струтктуру данных

int main(int argc, const char const *argv[])
{
     if(argc != 3)
    {
        printf("Few arguments. For example: <IP-adress> <PORT>\n");
        exit(EXIT_FAILURE);
    }

    socket_recv.server_port = atoi(argv[2]);
    strcpy(socket_recv.ip_adress, argv[1]);

    if(pthread_create(&threads_udp, NULL, Recv_UDP, NULL) != 0) Erorr_Pthread("Can't create UDP-pthread");
    if(pthread_join(threads_udp, NULL) != 0) Erorr_Pthread("Can't wait UDP-pthread");   
    return 0;
}
   

 void *Recv_UDP(void *arg)
 {

 	char buf[MAXBUF];
    int udp_socket;

 	addr.sin_family = AF_INET;
    addr.sin_port = htons(3031);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if((udp_socket = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
    Error_Socket("Can't create UDP-socket", &udp_socket);
    
/*Привязка адрессной струтуры к сокету*/
    if(bind(udp_socket, (struct sockaddr *)&addr, sizeof(addr)) == -1) 
    Error_Socket("Can't bind", &udp_socket);

 

  	while(1)
    {
        if(recvfrom(udp_socket, buf, sizeof(buf), 0, NULL, NULL) == -1) 
            Error_Socket("Can't read msg", &udp_socket); 
          
        udp_msg_status = atoi(buf);
       
        if(udp_msg_status == 1)
        {   
            if(pthread_create(&threads_tcp, NULL, Recv_TCP, &socket_recv) != 0) 
            Erorr_Pthread("Can't create UDP-pthread");
            if(pthread_join(threads_tcp, NULL) != 0) 
            Erorr_Pthread("Can't wait UDP-pthread");
        }

    }
   	
 }

  void *Recv_TCP(void *arg)
  {

    char *buff = (char*) malloc(MAXBUF);
    struct socket *data = (struct socket *) arg;
	int connect_socket;
    int bytes_data;

	/*Инициализация полей струтктуры аргументами с командной строки*/
	sockAddr.sin_family = AF_INET;  					
	sockAddr.sin_port = htons(data->server_port);	 		
	sockAddr.sin_addr.s_addr = inet_addr(data->ip_adress);	 


    if ((connect_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) 
        Error_Socket("Can't create socket", &connect_socket);

    if (connect(connect_socket, (struct sockaddr*) &sockAddr, sizeof (sockAddr)) == -1) 
        Error_Socket("Can't connect to server", NULL);
 
    bytes_data = recv(connect_socket, (char*)buff, MAXBUF, 0);
       // Error_Socket("Can't read", &connect_socket);


    proto_msg = amessage__unpack(NULL, bytes_data, buff);

    if (proto_msg == NULL)
        {
            printf("Error: Can't unpack\n");
            exit(1);
        }
    
    printf("Received message: %s\n", proto_msg->c);
    sleep(rand()%2);

    pthread_exit(0);
}

/*Обработка ошибок для потоков*/
void Erorr_Pthread(char *msg)
{   
    perror(msg);//вывод диагностического сообщения с описанием ошибки
    exit(EXIT_FAILURE);//преднамеренное завершение программы в случае ошибки
}

/*Обработчик ошибок для сокетов*/
void Error_Socket(char* msg, int *socket_fd)
{
    if(socket_fd == NULL)
    {
        perror(msg);
        exit(EXIT_FAILURE);
    }
    perror(msg);
    close(*socket_fd);
    exit(EXIT_FAILURE);
}