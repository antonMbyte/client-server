#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h> 
#include <stdlib.h> 
#include <pthread.h>
#include <errno.h>
#include <wait.h>
#include <stdbool.h> 
#include <linux/unistd.h>
#include <string.h>
#include "struct.h"
#include "interfaces.h"
#include "amessage.pb-c.h"

#define TRUE 1
#define FALSE 0
	
#define INTERVAL_TIME 2 /* - > переодичность отпраки датаграмм*/
#define MAXQUEUE 4 /* - > длина очереди сообщений*/

#define UDP_1_TYPE "0" /* - > статус для клиента 1 типа "жду сообщений"*/
#define UDP_2_TYPE "1" /* - > статус для клиента 2 типа "можно забирать"*/

#define PORTMAX 5	/* - > колличество портов для отправки датаграмм"*/

#define PORT_1_TYPE 3030
#define PORT_2_TYPE 3031


/*Делаем глобальный объект мьютекс, доступный всем потокам*/
pthread_mutex_t mutex;
pthread_mutex_t muteks;
/*Адрессные структуры для UDP и TCP*/
struct data send_msg_tcp;
struct sockaddr_in addr_udp;
struct sockaddr_in addr_tcp;

/*Динамическая длина очереди(меняеться в процессе выполнения программы)*/
int global_len = 0;
int global_status = 0;
/*Глобальный указатель на текущую очередь сообщений*/
struct trick *global_trick;

/*Позиция сотояния очереди для чтения из нее сообщений*/
int global_position = MAXQUEUE;

pthread_t threads_stcp;
AMessage *proto_msg;//указатель на распакованную струтктуру данных
AMessage send_proto_msg = AMESSAGE__INIT; //инициализируем сообщение из данных

int main(int argc, const char const *argv[])
{ 

	if(argc != 3)
	{
		printf("Few arguments. For example: TCP ports server -> <recv> <send>\n");
		exit(EXIT_FAILURE);
	}

	int tsp_port_recv_server = atoi(argv[1]);
	int tsp_port_send_server = atoi(argv[2]);

	/*Идентификаторы потоков*/
	pthread_t threads_udp;
 	pthread_t threads_rtcp; 
 	/*Создаем(инициализируем) объект мютекса*/
    pthread_mutex_init(&mutex, NULL);
   	pthread_mutex_init(&muteks, NULL);


	/*Создание потоков для датаграммного и потокового сокета*/
    if(pthread_create(&threads_udp, NULL, Send_UDP, NULL) != 0) Erorr_Pthread("Can't create UDP-pthread");
    if(pthread_create(&threads_rtcp, NULL, Receive_TCP_Accept, &tsp_port_recv_server) != 0) Erorr_Pthread("Can't create TCP-pthread");
  	if(pthread_create(&threads_stcp, NULL, Send_TCP_Accept, &tsp_port_send_server) != 0) Erorr_Pthread("Can't create TCP-pthread");

  	/*Присоединение потоков к главному и ождание завершения*/
  	if(pthread_join(threads_udp, NULL) != 0) Erorr_Pthread("Can't wait UDP-pthread");
    if(pthread_join(threads_rtcp, NULL) != 0) Erorr_Pthread("Can't wait TCP-pthread");
    if(pthread_join(threads_stcp, NULL) != 0) Erorr_Pthread("Can't wait TCP-pthread");

    /*Уничтожаем объект мютекс*/
    pthread_mutex_destroy(&mutex);
    return EXIT_SUCCESS;
}
	

/*Обработка ошибок потоков*/
void Erorr_Pthread(char *msg)
{	
	perror(msg);//вывод диагностического сообщения с описанием ошибки
	exit(EXIT_FAILURE);//преднамеренное завершение программы в случае ошибки
}

void *Send_UDP(void *arg)
{	
	
	int udp_socket;
	int bOptVal = TRUE;
	int bOptLen = sizeof(int);
	
	
    addr_udp.sin_family = AF_INET;
 	addr_udp.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	
	if((udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) 
		Error_Socket("Can't create UDP-socket", &udp_socket);
	
	setsockopt(udp_socket, SOL_SOCKET, SO_BROADCAST, (char*)&bOptVal, bOptLen);
	/*Шлем UDP-пакеты бесконечно*/	
	 while(1)
    {	
   			
   		if(global_len == 0)
   		{
    	/*Случай, когда в очереди нет сообщений(плюем в сеть датаграмму -> "Жду сообщения")*/
    		while(global_len != MAXQUEUE)
    		{
    			
    				addr_udp.sin_port = htons(PORT_1_TYPE); 
    				if((sendto(udp_socket, UDP_1_TYPE, sizeof(UDP_1_TYPE), 0, (struct sockaddr *)&addr_udp, sizeof(addr_udp))) == -1) 
    					Erorr_Pthread("Can't send msg");
 					sleep(INTERVAL_TIME);
    		}
    	
    		if((sendto(udp_socket, UDP_2_TYPE, sizeof(UDP_2_TYPE), 0, (struct sockaddr *)&addr_udp, sizeof(addr_udp))) == -1) 
    					Erorr_Pthread("Can't send msg");
    	}		

    	
    	
    	if (global_len == MAXQUEUE)
    	{
    	/*Случай, когда очередь переполнена(плюем в сеть датаграмму -> "Есть сообщения")*/
    		while(global_len != 0)
   			{	
    			
   					addr_udp.sin_port = htons(PORT_2_TYPE); 
    				if((sendto(udp_socket, UDP_2_TYPE, sizeof(UDP_2_TYPE), 0, (struct sockaddr *)&addr_udp, sizeof(addr_udp))) == -1) 
    					Erorr_Pthread("Can't send msg");
    				sleep(INTERVAL_TIME);
    		}
    		
    		if((sendto(udp_socket, UDP_1_TYPE, sizeof(UDP_1_TYPE), 0, (struct sockaddr *)&addr_udp, sizeof(addr_udp))) == -1) 
    					Erorr_Pthread("Can't send msg");	
		}			

    }	

 	close(udp_socket);
    return NULL;
 }


/*Потоковая функция для клиенских подключений*/
void *Receive_TCP_Accept(void *arg)
{	
	int listen_socket, accept_socket;
	
	int *port_recv = (int*)arg;

	/*Инициализация полей адрессной структуры для дальнейшей привязке к сокету*/
	addr_tcp.sin_family = AF_INET;  
	addr_tcp.sin_port = htons(*port_recv); 
	addr_tcp.sin_addr.s_addr = htonl(INADDR_ANY);
	
	/*Создание сокета (специального файла в ядре)*/
	if ((listen_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
		Error_Socket("Can't create socket", &listen_socket);
	
	/*Привязка адрессной струтуры к сокету*/
	if (bind(listen_socket, (struct sockaddr *)&addr_tcp, sizeof(addr_tcp)) == -1)
		Error_Socket("Can't bind", &listen_socket);
 

	/*Сокет начинает слушать извне запросы на соединения*/
	if (listen(listen_socket, SOMAXCONN) == -1)
		Error_Socket("Can't listen", &listen_socket);

	while(1)
	{
		pthread_t id;
		/*Сокет начинает принимать соединения и блокируеться если их нет*/
		if ((accept_socket = accept(listen_socket, 0, 0)) == -1)
			Error_Socket("Can't accept", &accept_socket);

		/*Каждое соединение обрабатываеться в новом потоке*/	
		if(pthread_create(&id, NULL, Receive_TCP, &accept_socket) != 0) 
			Erorr_Pthread("Can't create accept-ptread");


			/*Отпускаем поток в свободное плавание*/
			if(pthread_detach(id) != 0) 
			 	Erorr_Pthread("Can't detach pthread");
	}	
	
		
	return NULL;
}


/*Потоковая функция для отпраки сообщения клиенту 2 типа из очереди*/
void *Send_TCP_Accept(void *arg)
{	
	int listen_socket, accept_socket;
	
	int *port_send = (int*)arg;

	/*Инициализация полей адрессной структуры для дальнейшей привязке к сокету*/
	addr_tcp.sin_family = AF_INET;  
	addr_tcp.sin_port = htons(*port_send); 
	addr_tcp.sin_addr.s_addr = htonl(INADDR_ANY); 

	/*Создание сокета (специального файла в ядре)*/
	if ((listen_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) 
		Error_Socket("Can't create socket", &listen_socket);

	/*Привязка адрессной струтуры к сокету*/
	if (bind(listen_socket, (struct sockaddr *)&addr_tcp, sizeof(addr_tcp)) == -1) 
		Error_Socket("Can't bind", &listen_socket);
		

	/*Сокет начинает слушать извне запросы на соединения*/
	if (listen(listen_socket, SOMAXCONN) == -1)
		Error_Socket("Can't listen", &listen_socket);

	while(1)
	{
		pthread_t id;

		/*Сокет начинает принимать соединения и блокируеться если их нет*/
		if ((accept_socket = accept(listen_socket, 0, 0)) == -1)
			Error_Socket("Can't accept", &accept_socket);
		
	
		
		/*Создаем поток для нового соединеня и освобождаем сервер от обработки*/	
		if(pthread_create(&id, NULL, Send_TCP, &accept_socket) != 0) 
			Erorr_Pthread("Can't create accept-ptread");

			/*Отпускаем поток в свободное плавание*/
			if(pthread_detach(id) != 0) 
			 	Erorr_Pthread("Can't detach pthread");
		
	}	
	
		return NULL;
}


/*Функция для заполнения очереди сообщений*/
int Put_Queue(char *msg)
{
	struct trick *element = malloc(sizeof(struct trick));
	memcpy(element, msg, sizeof(struct trick));

	/*Если очередь заполнена, то уведомляем об этом и прикращаем прием*/
	if(global_len == MAXQUEUE) return -1;

	/*Если очередь пуста*/
	if(global_len == 0)
	{
		global_trick = element;
		global_trick->previous = NULL;
		global_trick->next = NULL;
		global_len++;
		return EXIT_SUCCESS;
	}

	/*Если очередь не пуста и не заполнена, то просто кладем сообщения*/
	global_trick->next = element;
	element->previous = global_trick;
	global_trick = element;
	global_len++;
	
	return EXIT_SUCCESS;

}


/*Функция для получения сообщений из очереди*/
char* Get_Queue(void)
{
	if (global_len == 0) return NULL;
	
	struct trick *tmp = global_trick;

	global_trick = global_trick->previous;
	
	global_len--;	
	return tmp->message;

}


/*Потоковая функция для клиенских подключений*/
void *Receive_TCP(void *socket_fd)
{


	int bytes_data;
	char *buff = (char*) malloc(MAXBUF);
	int *accept_socket = (int*) socket_fd;

	int status = 0;


	/*Чтение из сокет(из потока ввода/вывода по его идентификатору)*/
		if ((bytes_data = recv(*accept_socket, (char*)buff, MAXBUF, 0)) == -1);
		//Error_Socket("Can't read", accept_socket);
		
		proto_msg = amessage__unpack(NULL, bytes_data, buff); //распаковываем данные в струтктуру изначального размера и возвращаем ее адресс в памяти

		if (proto_msg == NULL)
		{
			printf("Error: Can't unpack\n");
			exit(1);
		}
		//struct data *data_recv = (struct data *)buffer_data;
		//strcpy(send_msg_tcp.string, data_recv->string); 
		
		pthread_mutex_lock(&mutex);
		status = Put_Queue(proto_msg->c);
		pthread_mutex_unlock(&mutex);
	
		if(status == -1)
		{	
			pthread_exit(0);
	 	}
		

    	printf("%30s\n", "Read Packet");
    	printf("Random time: %d sec\n", proto_msg->a);
    	printf("Random length: %d\n", proto_msg->b);
    	printf("Random string: %s\n", proto_msg->c);
    	printf("Size packet: %d bytes\n\n", bytes_data);
	 
	 	printf("Accepted message: %s\n", global_trick->message);
		printf("Write in queue: %d messages\n\n", global_len);

	amessage__free_unpacked(proto_msg,NULL);
	pthread_exit(0);
}



/*Потоковая функция для отпраки сообщения клиенту 2 типа из очереди*/
void *Send_TCP(void *arg)
{

	
	int *accept_socket = (int*) arg;
	char *msg;
	void *buff;
	int len, msg_len;

	pthread_mutex_lock(&muteks);
	msg = Get_Queue();
	pthread_mutex_unlock(&muteks);	


		if(msg == NULL)
		{
			pthread_exit(0);
		}

		msg_len = strlen(msg); //вычиляем длину строки взятой из очереди

		send_proto_msg.c = (char*)malloc(sizeof(char) * msg_len);//выделяем память в структуре под строку
		send_proto_msg.a = 0;
		send_proto_msg.b = 0;
		strcpy(send_proto_msg.c, msg); //копируем строку из очереди в поле структутры

		//printf("String = %s\n", msg);


		len = amessage__get_packed_size(&send_proto_msg); //получаем размер упакованной структуры 
    	buff = malloc(len); //выделяем память под этот размер струтктуры
    	amessage__pack(&send_proto_msg, buff); //сериализуем и кладем данные в эту память для дальнейшей передачи

		printf("Send message: %s\n", send_proto_msg.c);
		printf("Read from queue: %d messages\n\n", global_len);
		printf("Send %d bytes\n", len);

		
		//if (send(*accept_socket, buff, len, 0) == -1)
		send(*accept_socket, buff, len, 0);
		//	Error_Socket("Can't send", accept_socket);
			

	
		return NULL;
}


/*Обработчик ошибок для сокетов*/
void Error_Socket(char* msg, int *socket_fd)
{
	perror(msg);
	close(*socket_fd);
	exit(EXIT_FAILURE);
}