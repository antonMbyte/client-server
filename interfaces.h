/*Список интерфейсов (прототипов функций)*/
void Erorr_Pthread(char*);
void *Send_UDP(void *);
void *Receive_TCP_Accept(void *);
void *Send_TCP(void *arg);
void *Send_TCP_Accept(void *arg);
int Put_Queue(char *);
void *Receive_TCP(void*);
void Error_Socket(char*, int *);
char* Get_Queue(void);
void *Recv_UDP(void *arg);
void Generator_String(void);
void *Recv_TCP(void *arg);
void *Recv_UDP(void *arg);
