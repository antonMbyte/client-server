STANDART=-std=c99

#Ключи для линковщика. Для потоков и сериализации 
KEYS_PTHREAD = -pthread 
KEYS_PROTOBUF = -lprotobuf-c


all: protobuf-.c-.h server client_1_type client_2_type

#Генерируем c и h файлы заданной струтктуры даных для дальнейшей сериализации
protobuf-.c-.h:
	 protoc-c --c_out=. amessage.proto 

server:	server.c
	gcc $(KEYS_PTHREAD) $(STANDART) -o server server.c amessage.pb-c.c $(KEYS_PROTOBUF)	

client_1_type:	client_1_type.c
	gcc $(KEYS_PTHREAD) $(STANDART) -o client_1_type client_1_type.c amessage.pb-c.c $(KEYS_PROTOBUF)	

client_2_type:	client_2_type.c 
	gcc $(KEYS_PTHREAD) $(STANDART) -o client_2_type client_2_type.c amessage.pb-c.c $(KEYS_PROTOBUF)


clear:
	rm amessage.pb-c.h
	rm amessage.pb-c.c
	rm server
	rm client_2_type
	rm client_1_type