#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h> 
#include <stdlib.h> 
#include <pthread.h>
#include <errno.h>
#include <wait.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "struct.h"
#include "interfaces.h"
#include "amessage.pb-c.h"

#define PORT 3030
#define MAX_LEN 15
#define MIN_LEN 5


struct data msg_tcp;
struct socket socket_send;
struct sockaddr_in addr;
struct sockaddr_in sockAddr;

int udp_msg_status = -1;

pthread_t threads_udp;
pthread_t threads_tcp; 


AMessage proto_msg = AMESSAGE__INIT; //инициализируем сообщение из данных


int main(int argc, const char const *argv[])
{ 
    if(argc != 3)
    {
        printf("Few arguments. For example: <IP-adress> <PORT>\n");
        exit(EXIT_FAILURE);
    }


    socket_send.server_port = atoi(argv[2]);
    strcpy(socket_send.ip_adress, argv[1]);

    if(pthread_create(&threads_udp, NULL, Recv_UDP, NULL) != 0) Erorr_Pthread("Can't create TCP-pthread");
    if(pthread_join(threads_udp, NULL) != 0) Erorr_Pthread("Can't wait TCP-pthread");
    return 0;
}
   

 void *Recv_UDP(void *arg)
 {
 
    char buf[MAXBUF] = {0};
    int udp_socket = 0;

    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if((udp_socket = socket(AF_INET, SOCK_DGRAM, 0)) == -1) 
        Error_Socket("Can't create UDP-socket", &udp_socket);
    
    /*Привязка адрессной струтуры к сокету*/
    if(bind(udp_socket, (struct sockaddr *)&addr, sizeof(addr)) == -1) 
        Error_Socket("Can't bind", &udp_socket);
    
    srand ( time(NULL) );

    /*loop forever!!*/
    while(1)
        {
            if(recvfrom(udp_socket, buf, sizeof(buf), 0, NULL, NULL) == -1) 
                Error_Socket("Can't read msg", &udp_socket); 
            udp_msg_status = atoi(buf);
        
        /*if queue not complete, send messege*/
        if(udp_msg_status == 0)
        {

          Generator_String(); //generation random string

          if(pthread_create(&threads_tcp, NULL, Send_TCP, &socket_send) != 0) 
            Erorr_Pthread("Can't create TCP-pthread");
          if(pthread_join(threads_tcp, NULL) != 0) 
            Erorr_Pthread("Can't wait TCP-pthread");
        }
     
        }
    }

void *Send_TCP(void *arg)
{
    int connect_socket;
    int len;
    struct socket *data = (struct socket*) arg;
    proto_msg.a = rand()% 5 + 1; // заполянем поля струтктуры для протобуфа
    void *buff; //В этот буффер будем сериализовавывать данные(записывать последовательность упакованных бит,
//оптимизированных под минимальный размер сообщения)

  /*Инициализация полей струтктуры аргументами с командной строки*/
    sockAddr.sin_family = AF_INET;            
    sockAddr.sin_port = htons(data->server_port);     
    sockAddr.sin_addr.s_addr = inet_addr(data->ip_adress); 

    //PROTOBUF
    len = amessage__get_packed_size(&proto_msg); //получаем размер упакованной структуры 
    buff = malloc(len); //выделяем память под этот размер
    amessage__pack(&proto_msg, buff); //сериализуем и кладем данные в эту память для дальнейшей передачи
    
    if ((connect_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) 
        Error_Socket("Can't create socket", &connect_socket);

    if (connect(connect_socket, (struct sockaddr*) &sockAddr, sizeof (sockAddr)) == -1) 
        Error_Socket("Can't connect to server", NULL);
 
    /*Если UDP-пакет пришел, то отправить сообщение по TCP*/ 
   if (send(connect_socket, buff, len, 0) == -1)
        Error_Socket("Can't send", &connect_socket);

    printf("%30s\n", "Send Packet");
    printf("Random time: %d sec\n", proto_msg.a);
    printf("Random length: %d\n", proto_msg.b);
    printf("Random string: %s\n", proto_msg.c);
    printf("Size packet: %d bytes\n\n", len);
    
    free(buff);
    sleep(proto_msg.a);
    pthread_exit(0);
}


void Generator_String(void)
{
    char *buffer = NULL;
    char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890\0";    
    int i;
    srand ( time(NULL) ); 
    int string_length = rand()%MAX_LEN + MIN_LEN; //генерируем случайную длину строки

    proto_msg.b = string_length; //инициализируем поле структуры отвечающее за длину

    buffer = malloc(sizeof(char) * string_length); //выделяем память под строку
    
    proto_msg.c = (char*)malloc(sizeof(char) * string_length); //выделяем память под строку
     
     srand ( time(NULL) ); 
    
    //погнали заполнять
    for (i = 0; i <= string_length; i++)
    {  
        if(i == string_length) break;
        buffer[i] = alphabet[rand() % 62 + 0];
    }

    buffer[i] = '\0'; //указываем явно, что на выходе получаем строку
     
    strcpy(proto_msg.c, buffer); //copy string in structure PROTOBUF
    
    free(buffer); //освобождаем память под случайную строку

    return;
}


/*Обработка ошибок для потоков*/
void Erorr_Pthread(char *msg)
{   
    perror(msg);//вывод диагностического сообщения с описанием ошибки
    exit(EXIT_FAILURE);//преднамеренное завершение программы в случае ошибки
}

/*Обработчик ошибок для сокетов*/
void Error_Socket(char* msg, int *socket_fd)
{
    if(socket_fd == NULL)
    {
        perror(msg);
        exit(EXIT_FAILURE);
    }
    perror(msg);
    close(*socket_fd);
    exit(EXIT_FAILURE);
}